<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'My Yii Application';

$this->registerJs('
    $(".btn-transfer").click(function() {
        console.log($(this).parent().find(".amount").val());
        $.pjax.reload("#users");
    });
');
?>
    <div class="site-index">
        <div class="body-content">
            <div class="row">
                <?= GridView::widget([
                    'pjax' => true,
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id',
                        'status',
                        'username',
                        'balance',
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'header' => null,
                            'attribute' => 'amount',
                            'vAlign' => 'middle',
                            'width' => '210px',
                            'visible' => !Yii::$app->user->isGuest,
                            'readonly' => function($model) {
                                return ($model->id == Yii::$app->user->id);
                            },
                            'refreshGrid' => true,
                            'editableOptions' =>  [
                                'asPopover' => false,
                                'inputType' => \kartik\editable\Editable::INPUT_MONEY,
                                'displayValue' => 'Transfer',
                                'formOptions' => ['action' => ['/site/transfer']],
                            ],
                        ],

                    ],
                ]) ?>
            </div>
        </div>
    </div>
