<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['username', 'validateUsername'],
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username.
     *
     * @return bool
     * @throws Exception
     */
    public function validateUsername()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                //$this->addError($attribute, 'Incorrect username or password.');

                if (!(new User(['username' => $this->username]))->save())
                    throw new Exception('Model not saved');

                $this->getUser();
            }
        }

        return true;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), 3600*24*30);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|bool|null
     */
    public function getUser()
    {
        if ($this->_user === false || $this->_user === null) {
            $this->_user = User::find()->where(['username' => $this->username])->one();
        }

        return $this->_user;
    }
}
