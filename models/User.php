<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property float $balance
 * @property float $amount
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;

    public $amount;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['transfer'] = ['amount'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['username'], 'filter', 'filter' => '\yii\helpers\Html::encode'],
            [['balance', 'amount'], 'number', 'min' => 1, 'integerOnly' => false],
            [['balance', 'amount'], 'filter', 'filter' => 'floatval'],
            // ['amount', 'transfer'],
        ];
    }

    /**
     * @inheritDoc
     * @throws \Exception
     * @throws \Throwable
     */
    public function afterValidate()
    {
        parent::afterValidate();

        if ($this->scenario === 'transfer') {
            $transaction = \Yii::$app->db->beginTransaction();

            try {
                /** @var User $user */
                $user = User::findOne(['id' => \Yii::$app->user->id]);

                if ($user->balance <= -1) {
                    $this->addError('balance', 'Negative balance');
                }

                $this->balance = \Yii::$app->formatter->asDecimal($this->balance + $this->amount, 2);
                $user->balance = $user->balance - $this->amount;

                if (!$this->update(false) || !$user->update(false)) {
                    throw new Exception('Not updated');
                }

                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                \Yii::error($e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public function validateUsername($username)
    {
        foreach (static::findAll(['username' => $username]) as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
