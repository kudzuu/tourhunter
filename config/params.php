<?php

return [
    'adminEmail' => 'admin@example.com',
    'maskMoneyOptions' => [
        'thousands' => '',
        'decimal' => '.',
        'precision' => 2,
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
